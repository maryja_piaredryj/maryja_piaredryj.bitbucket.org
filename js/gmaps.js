function initialize() {
	var myLatLng = new google.maps.LatLng(53.9177233, 27.59500217);
    var mapOptions = {
        center: myLatLng,
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title:"БГУИР 1 корпус"
    });
}

