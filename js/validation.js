$(function() {
	$('#contactForm').validate({
		rules: {
			name: {
				required: true,
				maxlength: 35
			},
			email: {
				required: true,
				email: true,
				maxlength: 100
			},
			website: {
				required: false,
				url: true
			}
		},
		validClass: "valid",
		errorClass: "invalid",
		errorPlacement: function(error,element) { /*Removing adding error labels that crushes markup*/
		    return true;
		  }
	});
});